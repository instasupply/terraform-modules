resource "digitalocean_firewall" "fw-es" {
  name = "fw-es-${var.env}"

  tags = ["${var.env}-es"]

  inbound_rule = [
    {
      protocol         = "icmp"
      port_range       = "0"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "22"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "80"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "443"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol         = "tcp"
      port_range       = "9443"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol    = "tcp"
      port_range  = "9300"
      source_tags = ["${var.env}-es"]
    },
    {
      protocol    = "tcp"
      port_range  = "54328"
      source_tags = ["${var.env}-es"]
    },
  ]

  outbound_rule = [
    {
      protocol              = "tcp"
      port_range            = "0"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol              = "udp"
      port_range            = "0"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol              = "icmp"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
  ]
}
