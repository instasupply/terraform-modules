resource "digitalocean_droplet" "elasticsearch" {
  count              = "${var.esinstances}"
  image              = "${var.image}"
  name               = "es-${count.index}.${var.env}.instasupply.com"
  region             = "${var.region}"
  size               = "${var.size}"
  tags               = ["${var.env}", "elasticsearch", "${var.env}-es"]
  private_networking = "true"
  resize_disk        = "true"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["resize_disk"]
  }
}

resource "aws_route53_record" "arecord" {
  zone_id = "${var.route53_zone_id}"
  count   = "${var.esinstances}"
  name    = "es-${count.index}.${var.env}.instasupply.com"
  records = ["${element(digitalocean_droplet.elasticsearch.*.ipv4_address, count.index)}"]
  type    = "A"
  ttl     = "300"
}
