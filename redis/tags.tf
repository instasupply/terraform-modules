resource "digitalocean_tag" "env-redis" {
  name = "${var.env}-redis"
}

resource "digitalocean_tag" "env" {
  name = "${var.env}"
}
