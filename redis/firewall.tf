resource "digitalocean_firewall" "fw-redis" {
  name = "fw-redis-${var.env}"

  tags = ["${var.env}-redis"]

  inbound_rule = [
    {
      protocol         = "tcp"
      port_range       = "22"
      source_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol    = "tcp"
      port_range  = "6379"
      source_tags = ["${var.env}-web"]
    },
  ]

  outbound_rule = [
    {
      protocol              = "tcp"
      port_range            = "0"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol              = "udp"
      port_range            = "0"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol              = "icmp"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    },
  ]
}
