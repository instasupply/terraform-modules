resource "digitalocean_droplet" "redis" {
  image              = "${var.image}"
  name               = "${var.hostname}"
  region             = "${var.region}"
  size               = "${var.size}"
  tags               = ["${var.env}", "redis", "${var.env}-redis"]
  private_networking = "true"
  resize_disk        = "true"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["resize_disk"]
  }
}

