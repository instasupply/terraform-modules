variable "region" {
  description = "DigitalOcean Region. Defaults to lon1"
  default     = "lon1"
}

variable "env" {
  description = "Environment name. For example, qa or prod"
}

variable "lbtag" {
  description = "Tag of the Droplets you want to load balance"
}

variable "hostnameprefix" {
	description = "Hostname prefix for the DNS record. For example if app.instasupply.com hostnameprefix is app"
}

variable "r53zoneid" {
	description = "Zone ID for Route 53"
}

variable "r53zonename" {
	description = "Zone name for Route 53"
}
