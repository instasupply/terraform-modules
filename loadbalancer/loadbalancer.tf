resource "aws_route53_record" "lb" {
  zone_id = "${var.r53zoneid}"
  name    = "${var.hostnameprefix}.${var.r53zonename}"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_loadbalancer.web-lb.ipv4_address}"]
}

resource "digitalocean_loadbalancer" "web-lb" {
  name                   = "${var.env}-web-lb"
  region                 = "${var.region}"
  redirect_http_to_https = "false"
  algorithm              = "least_connections"

  forwarding_rule {
    entry_port      = 80
    entry_protocol  = "tcp"
    target_port     = 80
    target_protocol = "tcp"
  }

  forwarding_rule {
    entry_port      = 443
    entry_protocol  = "tcp"
    target_port     = 443
    target_protocol = "tcp"
  }

  forwarding_rule {
    entry_port      = 2812
    entry_protocol  = "tcp"
    target_port     = 2812
    target_protocol = "tcp"
  }

  forwarding_rule {
    entry_port      = 8443
    entry_protocol  = "http2"
    target_port     = 8443
    target_protocol = "http2"
  }

  forwarding_rule {
    entry_port      = 9009
    entry_protocol  = "tcp"
    target_port     = 9009
    target_protocol = "tcp"
  }

  sticky_sessions {
    type               = "cookies"
    cookie_name        = "DO-LB"
    cookie_ttl_seconds = "300"
  }

  healthcheck {
    port                = 80
    protocol            = "http"
    unhealthy_threshold = "3"
    healthy_threshold   = "10"
  }

  droplet_tag = "${var.lbtag}"
}
