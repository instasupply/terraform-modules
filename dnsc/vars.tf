variable "hostnameprefix" {
  description = "Hostname prefix for the C record. For example if eg.instasupply.com hostnameprefix is eg"
}

variable "destname" {
  description = "Destination name for the C record."
}

variable "route53_zone_name" {
  description = "Route53 Zone Name - your domain"
}

variable "route53_zone_id" {
  description = "Route 53 Zone ID - should come from your master/global state file"
}