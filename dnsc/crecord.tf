resource "aws_route53_record" "crecord" {
  zone_id = "${var.route53_zone_id}"
  name    = "${var.hostnameprefix}.${var.route53_zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${var.destname}"]
}
