resource "digitalocean_droplet" "web" {
  image              = "${var.image}"
  name               = "${var.hostname}"
  region             = "${var.region}"
  size               = "${var.size}"
  tags               = ["${var.env}", "${var.env}-web"]
  private_networking = "true"
  resize_disk        = "true"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["resize_disk"]
  }
}

resource "aws_route53_record" "arecord" {
  zone_id = "${var.route53_zone_id}"
  name    = "${digitalocean_droplet.web.name}"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.web.ipv4_address}"]
}
