resource "digitalocean_tag" "env-web" {
  name = "${var.env}-web"
}

resource "digitalocean_tag" "env" {
  name = "${var.env}"
}
