variable "region" {
  description = "DigitalOcean Region. Defaults to lon1"
  default     = "lon1"
}

variable "image" {
  description = "DigitalOcean Droplet Image. Defaults to Ubuntu 16"
  default     = "ubuntu-16-04-x64"
}

variable "size" {
  description = "DigitalOcean Droplet Size. Defaults to 1 VCPU, 1GB RAM"
  default     = "s-1vcpu-1gb"
}

variable "hostname" {
  description = "Hostname for the Droplet"
}

variable "env" {
  description = "Environment name. For example, qa or prod"
}

variable "route53_zone_id" {
  description = "Route53 Zone ID - should be taken from the Global State"
}
