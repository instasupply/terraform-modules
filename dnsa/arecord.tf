resource "aws_route53_record" "arecord" {
  zone_id = "${var.route53_zone_id}"
  name    = "${var.hostnameprefix}.${var.route53_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${var.ipaddress}"]
}
