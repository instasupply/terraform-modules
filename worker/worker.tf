resource "digitalocean_droplet" "worker" {
  image              = "${var.image}"
  name               = "${var.hostname}"
  region             = "${var.region}"
  size               = "${var.size}"
  tags               = ["${env}", "worker", "${var.env}-worker"]
  private_networking = "true"
  resize_disk        = "true"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["resize_disk"]
  }
}
