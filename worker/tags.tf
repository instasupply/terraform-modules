resource "digitalocean_tag" "env-worker" {
  name = "${var.env}-worker"
}

resource "digitalocean_tag" "env" {
  name = "${var.env}"
}
